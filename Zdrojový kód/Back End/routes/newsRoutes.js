const express = require("express");
const loggedModel = require("../models/loggedInModel");
const userModel = require("../models/userModel");
const newsModel = require("../models/newsModel");
const app = express();

function getDate(date){
    const year = date.getFullYear();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const hour = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const datum = day + "." + month + "." + year + " " + hour + ":" + minutes;
    return datum;
}

app.post('/addNews', async (req, res) => {
    const data = req.body;
    const admin = data.author;
    const datum = new Date();
    let date = getDate(datum);
    try {
        const user = await loggedModel.findOne({nickname: admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: admin, role: "admin"});
            if (isAdmin.length !== 0) {
                const news = new newsModel(req.body);
                news.date = date;
                await news.save();
                res.status(200).json("Novinka úspěšně přidána.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při pčidávání novinky!");
    }
});

app.get('/getNews', async(req, res) =>{
    try {
        const news = await newsModel.find();
        res.status(200).json(news);
    } catch {
        res.status(400).json("Chyba při načítání novinek!");
    }
});

app.post('/deleteNews', async(req, res) =>{
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.admin, role: "admin"});
            if (isAdmin.length !== 0) {
                await newsModel.deleteMany({_id: data.id});
                res.status(200).json("Novinka úspěšně odstraněna.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při odstranění novinky!");
    }
});

app.post('/updateNews', async(req, res) => {
    const data = req.body;
    const admin = data.nickname;
    const datum = new Date();
    let date = getDate(datum);
    console.log(data);
    console.log('here');
    try {
        const user = await loggedModel.findOne({nickname: admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: admin, role: "admin"});
            if (isAdmin.length !== 0) {
                console.log('here now');
                await newsModel.updateOne({_id: data.id}, {postName: data.name, text: data.text, picture: data.picture, updateAuthor: data.nickname, updateDate: date});
                res.status(200).json("Novinka úspěšně změněna.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při updatu novinky!");
    }
});

module.exports = app;