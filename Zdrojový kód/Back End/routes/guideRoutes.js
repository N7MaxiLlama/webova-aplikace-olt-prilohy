const express = require("express");
const loggedModel = require("../models/loggedInModel");
const userModel = require("../models/userModel");
const vehicleModel = require("../models/vehicleGuideModel");
const infantryModel = require("../models/infantryGuideModel");
const strategicModel = require("../models/strategicGuideModel");
const app = express();

function getDate(date) {
    const year = date.getFullYear();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const hour = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const datum = day + "." + month + "." + year + " " + hour + ":" + minutes;
    return datum;
}

app.post('/addVehicleGuide', async(req, res) => {
   const data = req.body;
   const datum = new Date();
   let date = getDate(datum);
   data.date = date;
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.author});
            if (canPost.length !== 0 || canPost[0].role === 'admin' || canPost[0].role === 'member') {
                const vehicle = new vehicleModel(data);
                vehicle.save();
                res.status(200).json(vehicle._id);
            } else {
                res.status(400).json("Uživatel nemá práva k postování návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.get('/getVehicleGuides', async(req, res) => {
    try {
      const guides = await vehicleModel.find();
      res.status(200).json(guides);
    } catch {
      res.status(400).json("Chyba při načítání návodů!");
    }
});

app.post('/vehicleDetail', async(req, res) => {
    const data = req.body;
    try {
       const guides = await vehicleModel.findOne({_id: data.ID});
       res.status(200).json(guides);
   } catch{
       res.status(400).json("Chyba při načítání návodu!");
   }
});

app.post('/updateVehicleGuide', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.updateDate = date;
    try {
        const user = await loggedModel.findOne({nickname: data.updateAuthor});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.updateAuthor});
            if (canPost.length !== 0 || canPost[0].role === 'admin') {
                await vehicleModel.updateOne({_id: data.id}, {updateAuthor: data.updateAuthor,
                updateDate: data.updateDate, name: data.name, description: data.description,
                text: data.text, weapon: data.weapon, harvest: data.harvest, topGun: data.topGun,
                secondTopGun: data.secondTopGun, wings: data.wings, bellyGun: data.bellyGun, tailGun: data.tailGun,
                leftGun: data.leftGun, rightGun: data.rightGun, frontLeftGun: data.frontLeftGun, frontRightGun: data.frontRightGun,
                backLeftGun: data.backLeftGun, backRightGun: data.backRightGun, utility: data.utility, defence: data.defence, performance: data.performance,
                logistics: data.logistics});
                res.status(200).json('Návod úspěšně upraven.');
            } else {
                res.status(400).json("Uživatel nemá práva k upravení návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post ('/deleteVehicleGuide', async(req, res) => {
   const data = req.body;
   try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.nickname});
            if (canPost.length !== 0 || canPost[0].role === 'admin') {
                await vehicleModel.deleteOne({_id: data.id});
                res.status(200).json('Návod úspěšně odstraněn.');
            } else {
                res.status(400).json("Uživatel nemá práva k upravení návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/addInfantryGuide', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.author});
            if (canPost.length !== 0 || canPost[0].role === 'admin' || canPost[0].role === 'member') {
                const infantry = new infantryModel(data);
                infantry.save();
                res.status(200).json(infantry._id);
            } else {
                res.status(400).json("Uživatel nemá práva k postování návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.get('/getInfantryGuides', async(req, res) => {
    try {
        const guides = await infantryModel.find();
        res.status(200).json(guides);
    } catch {
        res.status(400).json("Chyba při načítání návodů!");
    }
});

app.post('/infantryDetail', async(req, res) => {
    const data = req.body;
    try {
        const guides = await infantryModel.findOne({_id: data.ID});
        res.status(200).json(guides);
    } catch{
        res.status(400).json("Chyba při načítání návodu!");
    }
});

app.post ('/deleteInfantryGuide', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.nickname});
            if (canPost.length !== 0 || canPost[0].role === 'admin') {
                await infantryModel.deleteOne({_id: data.id});
                res.status(200).json('Návod úspěšně odstraněn.');
            } else {
                res.status(400).json("Uživatel nemá práva k upravení návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/updateInfantryGuide', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.updateDate = date;
    try {
        const user = await loggedModel.findOne({nickname: data.updateAuthor});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.updateAuthor});
            if (canPost.length !== 0 || canPost[0].role === 'admin') {
                await infantryModel.updateOne({_id: data.id}, {updateAuthor: data.updateAuthor,
                    updateDate: data.updateDate, name: data.name, description: data.description,
                    text: data.text, weapon: data.weapon, secondatyWeapon: data.secondaryWeapon,
                tool: data.tool, ability: data.ability, suit: data.suit, grenade: data.grenade,
                utility: data.utility, melee: data.melee, implant: data.implant, secondImplant: data.secondImplant,
                tactical: data.tactical, image: data.image});
                res.status(200).json('Návod úspěšně upraven.');
            } else {
                res.status(400).json("Uživatel nemá práva k upravení návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/addStrategicGuide', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.author});
            if (canPost.length !== 0 || canPost[0].role === 'admin' || canPost[0].role === 'member') {
                const strategy = new strategicModel(data);
                strategy.save();
                res.status(200).json(strategy._id);
            } else {
                res.status(400).json("Uživatel nemá práva k postování návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.get('/getStrategicGuides', async(req, res) => {
    try {
        const guides = await strategicModel.find();
        res.status(200).json(guides);
    } catch {
        res.status(400).json("Chyba při načítání návodů!");
    }
});

app.post('/strategicDetail', async(req, res) => {
    const data = req.body;
    try {
        const guides = await strategicModel.findOne({_id: data.ID});
        res.status(200).json(guides);
    } catch{
        res.status(400).json("Chyba při načítání návodu!");
    }
});

app.post ('/deleteStrategicGuide', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.nickname});
            if (canPost.length !== 0 || canPost[0].role === 'admin') {
                await strategicModel.deleteOne({_id: data.id});
                res.status(200).json('Návod úspěšně odstraněn.');
            } else {
                res.status(400).json("Uživatel nemá práva k upravení návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/updateStrategicGuide', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.updateDate = date;
    try {
        const user = await loggedModel.findOne({nickname: data.updateAuthor});
        if (user.length !== 0){
            const canPost = await userModel.find({nickname: data.updateAuthor});
            if (canPost.length !== 0 || canPost[0].role === 'admin') {
                await strategicModel.updateOne({_id: data.id}, {updateAuthor: data.updateAuthor,
                    updateDate: data.updateDate, name: data.name, description: data.description,
                    text: data.text, image: data.image});
                res.status(200).json('Návod úspěšně upraven.');
            } else {
                res.status(400).json("Uživatel nemá práva k upravení návodu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

module.exports = app;