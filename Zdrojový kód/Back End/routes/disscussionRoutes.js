const express = require("express");
const app = express();
const discussionModel = require("../models/discussionModel");
const userModel = require("../models/userModel");
const loggedModel = require("../models/loggedInModel");
const replyModel = require("../models/discussionReplyModel");
const replyReport = require("../models/replyReportModel");
const themeReport = require("../models/themeReportModel");

function getDate(date) {
    const year = date.getFullYear();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const hour = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const datum = day + "." + month + "." + year + " " + hour + ":" + minutes;
    return datum;
}

app.post('/addTheme', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    data.lastReply = null;
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const theme = new discussionModel(data);
            await theme.save();
            res.status(200).json(theme._id);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při vkládání tématu!");
    }
});

app.post('/getThemes', async(req, res) => {
   const data = req.body;
   try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const themes = await discussionModel.find();
            res.status(200).json(themes);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
   } catch {
        res.status(400).json("Chyba při načítání témat!");
   }
});

app.post('/getThemeDetail', async(req, res) => {
   const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const detail = await discussionModel.findOne({_id: data.id});
            res.status(200).json(detail);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při načítání témat!");
    }
});

app.post('/updateTheme', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.updateDate = date;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canUpdate = await userModel.find({nickname: data.nickname});
            if (canUpdate.length !== 0 || canUpdate[0].role==='admin') {
                await discussionModel.updateOne({_id: data.id}, {text: data.text, name: data.name, updateDate: data.updateDate, updateAuthor: data.nickname});
                res.status(200).json("Téma úspěšně upraveno.");
            } else {
                res.status(400).json("Uživatel nemá práva k upravení tématu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/deleteTheme', async(req, res) => {
    const data = req.body;
    console.log(data);
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                await discussionModel.deleteOne({_id: data.id})
                await replyModel.deleteMany({discussionID: data.id})
                res.status(200).json("Téma úspěšně upraveno.");
            } else {
                res.status(400).json("Uživatel nemá práva k upravení tématu.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/addDiscussionComment', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    console.log(data);
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const comment = new replyModel(data);
            await comment.save();
            await discussionModel.updateOne({_id: data.discussionID}, {lastReply: data.date});
            res.status(200).json('Komentář úspěšně přidán.');
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při přidání komentáře!");
    }
});

app.post('/getDiscussionComments', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const comments = await replyModel.aggregate([
                { $match: {
                        discussionID: data.id
                    }
                },
                {   $lookup: {
                        from: 'users',
                        localField: 'author',
                        foreignField: 'nickname',
                        as: 'user'
                    }
                }]);
            res.status(200).json(comments);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při načítání komentářů!");
    }
});

app.post('/updateDiscussionComment', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canUpdate = await userModel.find({nickname: data.nickname});
            if (canUpdate.length !== 0 || canUpdate[0].role==='admin') {
                await replyModel.updateOne({_id: data.id}, {text: data.text, updateDate: data.date});
                await discussionModel.updateOne({_id: data.discussionID}, {lastReply: data.date});
                res.status(200).json("Komentář úspěšně upraven.");
            } else {
                res.status(400).json("Uživatel nemá práva k upravení komentáře.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/deleteDiscussionComment', async(req, res) => {
   const data = req.body;
   console.log(data);
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canUpdate = await userModel.find({nickname: data.nickname});
            if (canUpdate.length !== 0 || canUpdate[0].role==='admin') {
                await replyModel.deleteOne({_id: data.id});
                res.status(200).json("Komentář úspěšně odstraněn.");
            } else {
                res.status(400).json("Uživatel nemá práva k odstranění komentáře.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/reportReply', async(req,res) => {
   const data = req.body;
   const datum = new Date();
   let date = getDate(datum);
   data.reportDate = date;
   data.status = 'Podáno';
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const report = new replyReport(data);
            report.save();
            res.status(200).json(' Report úspěšně podán.');
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při postování reportu!");
    }
});

app.post('/reportTheme', async(req, res) => {
   const data = req.body;
   const datum = new Date();
   let date = getDate(datum);
   data.reportDate = date;
   data.status = 'Podáno';
    try {
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0){
            const report = new themeReport(data);
            console.log(report);
            report.save();
            res.status(200).json('Report úspěšně podán.');
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při postování reportu!");
    }
});

app.post('/getReplyReports', async(req, res) => {
    console.log('here')
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const reports = await replyReport.find({status: 'Podáno'});
                console.log(reports);
                res.status(200).json(reports);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getThemeReports', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const reports = await themeReport.find({status: 'Podáno'});
                res.status(200).json(reports);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getCompletedReplyReports', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const reports = await replyReport.find({$or: [{status: 'Vyřízeno'}, {status: 'Příspěvěk odstraněn'}]});
                res.status(200).json(reports);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getCompletedThemeReports', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const reports = await themeReport.find({$or: [{status: 'Vyřízeno'}, {status: 'Příspěvěk odstraněn'}]});
                res.status(200).json(reports);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getReportReplyDetail', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const reports = await replyReport.findOne({_id: data.id});
                res.status(200).json(reports);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getReportThemeDetail', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const reports = await themeReport.findOne({_id: data.id});
                res.status(200).json(reports);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/changeReportReplyState', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                await replyReport.updateOne({_id: data.id}, {status: data.state});
                res.status(200).json('Update úspěšný');
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/changeReportThemeState', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                await themeReport.updateOne({_id: data.id}, {status: data.state});
                res.status(200).json('Update úspěšný');
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

module.exports = app;