const express = require("express");
const app = express();
const userModel = require("../models/userModel");
const loggedModel = require("../models/loggedInModel");
const axios = require("axios");

async function getName(id){
    const response = await axios.get('https://census.daybreakgames.com/s:N7MaxiLlama/json/get/ps2:v2/character/' + id);
    return name = response.data.character_list[0].name.first;
}

app.post('/getOnlineVS', async(req, res) => {
    try {
        const response = await axios.get('https://census.daybreakgames.com/s:N7MaxiLlama/json/get/ps2:v2/outfit/37547243563126071/?c:resolve=member_online_status');
        let onlineVS = response.data;
        const max = onlineVS.outfit_list[0].member_count;
        const members = onlineVS.outfit_list[0].members;
        let characters = [];
        for (let i = 0; i < max; i++){
            if (members[i].online_status === '10') {
                let name = await getName(members[i].character_id);
                let character = {nickname: name, state: 'online'};
                characters.push(character);
            }
        }
        res.status(200).json(characters);
    } catch {
        res.status(400).json("Chyba při napojení na Daybreak API");
    }
});

app.post('/getOnlineTR', async(req, res) => {
    try {
        const response = await axios.get('https://census.daybreakgames.com/s:N7MaxiLlama/json/get/ps2:v2/outfit/37558537335609351/?c:resolve=member_online_status');
        let onlineTR = response.data;
        const max = onlineTR.outfit_list[0].member_count;
        const members = onlineTR.outfit_list[0].members;
        let characters = [];
        for (let i = 0; i < max; i++){
            if (members[i].online_status === '10'){
                let name = await getName(members[i].character_id);
                let character = {nickname: name, state: 'online'};
                characters.push(character);
            }
        }
        res.status(200).json(characters);
    } catch {
        res.status(400).json("Chyba při napojení na Daybreak API");
    }
});

app.post('/getOnlineAdmins', async(req, res) => {
    try {
        const users = await loggedModel.aggregate([
            { $match: {

                }
            },
            {   $lookup: {
                    from: 'users',
                    localField: 'nickname',
                    foreignField: 'nickname',
                    as: 'user'
                }
            }]);
        let online = [];
        let person = [];
        for (let i = 0; i < users.length; i++){
            person[i] = users[i].user[0];
        }
        for (let i = 0; i < person.length; i++){
            if (person[i].role === 'admin'){
                online.push(person[i].nickname);
            }
        }
        let trulyOnline = [...new Set(online)];
        res.status(200).json(trulyOnline);
    } catch {
        res.status(400).json('Chyba při získávání přihlášených uživatelů.');
    }
});

app.post('/getOnlineMembers', async(req, res) => {
    try {
        const users = await loggedModel.aggregate([
            { $match: {

                }
            },
            {   $lookup: {
                    from: 'users',
                    localField: 'nickname',
                    foreignField: 'nickname',
                    as: 'user'
                }
            }]);
        let online = [];
        let person = [];
        for (let i = 0; i < users.length; i++){
            person[i] = users[i].user[0];
        }
        for (let i = 0; i < person.length; i++){
            if (person[i].role === 'member'){
                online.push(person[i].nickname);
            }
        }
        let trulyOnline = [...new Set(online)];
        res.status(200).json(trulyOnline);
    } catch {
        res.status(400).json('Chyba při získávání přihlášených uživatelů.');
    }
});

app.post('/getOnlineUsers', async(req, res) => {
    try {
        const users = await loggedModel.aggregate([
            { $match: {

                }
            },
            {   $lookup: {
                    from: 'users',
                    localField: 'nickname',
                    foreignField: 'nickname',
                    as: 'user'
                }
            }]);
        let online = [];
        let person = [];
        for (let i = 0; i < users.length; i++){
            person[i] = users[i].user[0];
        }
        for (let i = 0; i < person.length; i++){
            if (person[i].role === 'user'){
                online.push(person[i].nickname);
            }
        }
        let trulyOnline = [...new Set(online)];
        res.status(200).json(trulyOnline);
    } catch {
        res.status(400).json('Chyba při získávání přihlášených uživatelů.');
    }
});

module.exports = app;