const express = require("express");
const userModel = require("../models/userModel");
const loggedModel = require("../models/loggedInModel");
const newsModel = require("../models/newsModel");
const codesModel = require("../models/codeModel");
const app = express();
const crypto = require("crypto");
const {passwordConfig, jwtConfig} = require("../config");
const jwt = require("jsonwebtoken");


function hash(hashMe){
    return crypto.pbkdf2Sync(
        hashMe,
        passwordConfig.salt,
        passwordConfig.iterations,
        passwordConfig.keylen,
        passwordConfig.digest
    ).toString('hex');
}

function generateToken(user){
    const tokenPayload = {
        nickname: user.nickname,
        role: user.role,
        id: user._id,
        profilePicture: user.profilePicture,
        VS: user.VS,
        TR: user.TR,
        NC: user.NC,
        NS: user.NS,
        aboutMe: user.aboutMe,
    };
    return jwt.sign(
        tokenPayload,
        jwtConfig.secret,
        {
            algorithm: jwtConfig.algorithms[0]
        }
    );
}

function getDate(date){
    const year = date.getFullYear();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const hour = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const datum = day + "." + month + "." + year + " " + hour + ":" + minutes;
    return datum;
}

function getNewsText(nickname, faction, role){
    var factionText;
    var roleText;
    if (role === 'admin') {
        roleText = 'velitelský kód';
    }  else {
        roleText = 'ověřovací kód'
    }
    if (faction === 'VS'){
        factionText = "Uživatel " + nickname + " použil " + roleText + ' Suverenity Vanu a je připraven vztyčit náš baner na celém Auraxis a ukázat světlo Vanu všem bezvěrcům.\n\nTechnologie se rovná síle!';
    } else if (faction === 'TR') {
        factionText = "Uživatel " + nickname + " použil " + roleText + ' Terranské republiky a je připraven potlačit povstání na Auraxis.\n\nLoajalita do smrti, síla v jednotě!';
    } else if (faction === 'NC') {
        factionText = "Uživatel " + nickname + " použil " + roleText + ' Nového konglomerátu a je připraven bojovat za svobodu každého muže, ženy i dítěte na Auraxis.';
    } else {
        factionText = "Uživatel " + nickname + " použil " + roleText + ' Nanite Systems.\n\n01010100 01100001 01101000 01101100 01100101 00100000 01101010 01100101 ' +
            '01100100 01101110 01101111 01110100 01101011 01100001 00100000 01101010 01100101 00100000 01110000 11000101 10011001 01101001 01110000 01110010 01100001 ' +
            '01110110 01100101 01101110 01100001 00100000 01110011 01101100 01101111 01110101 11000101 10111110 01101001 01110100 00100000 01101011 01101111 01101101 ' +
            '01110101 01101011 01101111 01101100 01101001 01110110 00101100 00100000 01101011 01100100 01101111 00100000 01111010 01100001 01110000 01101100 01100001 ' +
            '01110100 11000011 10101101 00100000 01101110 01100101 01101010 01110110 11000011 10101101 01100011 01100101 00101110 00100000 01000100 01101111 01101011 ' +
            '01110101 01100100 00100000 01101110 01100101 01101110 01100001 01110011 01110100 01100001 01101110 01100101 00100000 01101010 01100101 01101010 11000011 ' +
            '10101101 00100000 11000100 10001101 01100001 01110011 00101110 00100000 01010010 01101111 01100010 01101111 01110100 01101001 00100000 01110000 01101111 ' +
            '01110110 01110011 01110100 01100001 01101110 01101111 01110101 00100000 01100001 00100000 01101101 01100001 01110011 01101111 01110110 11000011 10101001 ' +
            '00100000 01110011 11000011 10100001 11000100 10001101 01101011 01111001 00100000 01110000 11000101 10011001 01100101 01110011 01110100 01100001 01101110 ' +
            '01101111 01110101 00100000 01100101 01111000 01101001 01110011 01110100 01101111 01110110 01100001 01110100 00101110 00100000 01000010 01100101 ' +
            '01100101 01110000 00100000 01100010 01101111 01101111 01110000 00100000 01100010 01100101 01100101 01110000 00101110!';
    }
    if (role === 'admin' && faction === 'VS'){
        factionText = factionText + '\n\nProsím, prokažte veliteli úctu, jakou si Llamas Chosen a Paragon Suverenity Vanu zasluhuje.';
    } else if (role === 'admin' && faction === 'TR') {
        factionText = factionText + '\n\nProsím, prokažte veliteli úctu, jakou si Praeceptorem LLama a Polní Maršál Terranské republiky zasluhuje.';
    } else if (role === 'admin' && faction === 'NC') {
        factionText = factionText + '\n\nProsím, prokažte veliteli úctu, jakou si Vykonavatel Nového Konglomerátu zasluhuje.';
    } else if (role === 'admin' && faction === 'NS') {
        factionText = factionText + '\n\nProsím, prokažte veliteli úctu, jakou si software Nanite Systems verze 1.0.0 zasluhuje.';
    }
    return factionText;
}

app.post("/add_user", async (request, response) => {
    const user = new userModel(request.body);
    user.password = hash(user.password);
    user.securityAnswer = hash(user.securityAnswer);
    try {
        await user.save();
        response.send(user);
        console.log("Registroval se uživatel: " + user);
    } catch (error) {
        response.status(500).send(error);
    }
});

app.post("/users", async (req, res) => {
    try {
        const data = req.body;
        const nick = data.nickname;
        const isLogged = await loggedModel.find({nickname: nick});
        if (isLogged.length !== 0){
            const isAdmin = await userModel.find({nickname: nick, role: "admin"});
            if (isAdmin.length !== 0) {
                const users = await userModel.find({nickname: {$ne: nick}});
                res.status(201).json(users);
            } else {
                res.status(400).send("Uživatel zadávající dotaz není admin!");
            }
        } else  {
            res.status(400).send("Uživatel není přihlášen!");
        }
    } catch (error) {
        res.status(400).send("Chyba při hledání uživatelů!");
    }
});

app.post("/login", async (req, res) =>{
    try {
        const data = req.body;
        const nick = data.nickname;
        const pass = hash(data.password);
        const user = await userModel.findOne({ nickname: nick, password: pass});
        const loggedIn = new loggedModel(req.body);
        const response = {
            token: generateToken(user)
        };
        if(user.length !== 0) {
            try {
                await loggedIn.save();
            } catch (error) {
                response.status(500).send(error);
            }
        }
        res.status(201).json(response);
    } catch {
        res.status(400).send("Neexistující účet!")
    }
});

app.post("/logout", async(req, res) => {
    try {
        const data = req.body;
        const nick = data.nickname;
        await loggedModel.deleteOne({nickname: nick});
        res.status(201).json("Odhlášení úspěšné.")
    } catch {
        res.status(400).send("Chyba při odhlášení!");
    }
});

app.post("/changeNickname", async (req, res) =>{
    try {
        const data = req.body;
        const oldNickname = data.oldNickname;
        const newNickname = data.nickname;
        const security = hash(data.question);
        const userLoggedIn = await loggedModel.findOne({nickname: oldNickname});
        if (userLoggedIn.length !== 0) {
            const user = await userModel.findOne({nickname: oldNickname, securityAnswer: security});
            if (user.length !== 0) {
                await loggedModel.updateOne({nickname: oldNickname},{nickname:newNickname});
                await userModel.updateOne({nickname: oldNickname},{nickname:newNickname});
                res.status(201).json("Změna přezdívky úspěšná.")
            } else {
                res.status(400).json("Špatně zadaná bezpečnostní otázka!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen! ");
        }
    } catch {
        res.status(400).json("Chyba při změně přezdívky!");
    }
});

app.post("/changeAvatar", async (req, res) =>{
    try {
        const data = req.body;
        const nick = data.nickname;
        const picture = data.picture;
        const user = await loggedModel.findOne({nickname: nick});
        if (user.length !== 0) {
            await userModel.updateOne({nickname: nick}, {profilePicture: picture});
            res.status(200).json("Avatar úspěšně změněn.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při změně avatara!")
    }
});

app.post("/changePassword", async (req, res) => {
    try {
        const data = req.body;
        const nick = data.nickname;
        const answer = hash(data.answer);
        const newPass = hash(data.password);
        const user = await userModel.findOne({nickname: nick, securityAnswer: answer});
        if (user.length !== 0){
            await userModel.updateOne({nickname: nick}, {password: newPass});
            res.status(200).json("Heslo úspěšně změněno.");
        } else {
            res.status(400).json("Uživatel s danou přezdívkou a bezpečnostní otázkou neexistuje!")
        }
    } catch {
        res.status(400).json("Chyba při změně hesla!");
    }
});

app.post("/checkNickname", async (req, res) => {
    try {
        const data = req.body;
        const nick = data.nickname;
        const user = await userModel.findOne({nickname: nick});
        if (user == null){
            res.status(200).json(true);
        } else {
            res.status(200).json(false);
        }
    } catch {
        res.status(400).json("Chyba při kontrole přezdívky!");
    }
});


app.post("/deleteAccount", async (req, res) => {
    try {
        const data = req.body;
        const nick = data.nickname;
        const pass = hash(data.password);
        const user = await userModel.findOne({nickname: nick, password: pass});
        if (user.length !== 0) {
            await userModel.deleteMany({nickname: nick, password: pass});
            res.status(200).json("Účet úspěšně smazán.");
        } else {
            res.status(400).json("Špatně zadané heslo!");
        }
    } catch {
        res.status(400).json("Chyba při odstranění účtu!");
    }
});

app.post("/changeRole", async (req, res) => {
    const data = req.body;
    const newRole = data.role;
    const admin = data.admin;
    const nickname = data.nick;
    try {
        const user = await loggedModel.findOne({nickname: admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: admin, role: "admin"});
            if (isAdmin.length !== 0){
                await userModel.updateOne({nickname: nickname}, {role: newRole});
                res.status(200).json("Role úspěšně změněna.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při změně role!");
    }
});

app.post("/deleteUserByAdmin", async (req, res) => {
    const data = req.body;
    const admin = data.admin;
    const nick = data.nick;
    try {
        const user = await loggedModel.findOne({nickname: admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: admin, role: "admin"});
            if (isAdmin.length !== 0) {
                await userModel.deleteMany({nickname: nick});
                await loggedModel.deleteMany({nickname: nick});
                res.status(200).json("Uživatel úspěšně odstraněn.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba přiodstranění uživatele!");
    }
});

app.post('/saveCode', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.admin, role: "admin"});
            if (isAdmin.length !== 0) {
                const newCode = new codesModel(req.body);
                await newCode.save();
                res.status(200).json("Kód úspěšně uložen.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při updatu novinky!");
    }
});

app.post('/getCodes', async(req, res) => {
    try {
        const data = req.body;
        const nick = data.nickname;
        const isLogged = await loggedModel.find({nickname: nick});
        if (isLogged.length !== 0){
            const isAdmin = await userModel.find({nickname: nick, role: "admin"});
            if (isAdmin.length !== 0) {
                const codes = await codesModel.find();
                res.status(201).json(codes);
            } else {
                res.status(400).send("Uživatel zadávající dotaz není admin!");
            }
        } else  {
            res.status(400).send("Uživatel není přihlášen!");
        }
    } catch (error) {
        res.status(400).send("Chyba při hledání kódů!");
    }
});

app.post('/deleteCode', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.admin});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.admin, role: "admin"});
            if (isAdmin.length !== 0) {
                await codesModel.deleteMany({_id: data.id});
                res.status(200).json("Kód úspěšně odstraněn.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při odstranění kódu!");
    }
});

app.post('/authenticateMe', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const codeAuth = await codesModel.findOne({code: data.code});
            if (codeAuth.length !== 0){
                await userModel.updateOne({nickname: data.nickname}, {role: codeAuth.role});
                const news = new newsModel(req.body);
                const datum = new Date();
                news.date = getDate(datum);
                news.author = 'Automatické ověření';
                news.postName = 'Vítej, ' + data.nickname + '!';
                news.picture = codeAuth.faction + '.png';
                news.text = getNewsText(data.nickname, codeAuth.faction, codeAuth.role);
                news.updateAuthor = null;
                news.updateDate = null;
                await news.save();
                const newRole = codeAuth.role;
                res.status(200).json(newRole);
            } else {
                res.status(400).json("Daný kód neexistuje!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});


app.post('/updateCharacters', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            await userModel.updateOne({nickname: data.nickname}, {VS: data.VS, TR: data.TR, NC: data.NC, NS: data.NS});
            res.status(200).json("Postavy úspěšně uloženy.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ukládání postav!");
    }
});

app.post('/updateABoutMe', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            await userModel.updateOne({nickname: data.nickname}, {aboutMe: data.aboutMe});
            res.status(200).json("Postavy úspěšně uloženy.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ukládání postav!");
    }
});

module.exports = app;