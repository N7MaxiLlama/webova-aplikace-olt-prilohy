const express = require("express");
const userModel = require("../models/userModel");
const app = express();

app.post('/getAboutUs', async(req, res) => {
    const data = req.body;
    try {
        const user = await userModel.findOne({nickname: data.nickname});
        user.password = null;
        user.securityAnswer = null;
        res.status(200).json(user);
    } catch {
        res.status(400).json("Chyba při načítání sekce o nás!");
    }
});

module.exports = app;