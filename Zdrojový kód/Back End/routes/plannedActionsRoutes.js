const express = require("express");
const loggedModel = require("../models/loggedInModel");
const userModel = require("../models/userModel");
const actionsModel = require("../models/actionsModel");
const attendeeModel = require("../models/attendeesModel");
const actionCommentModel = require("../models/actionsCommentsModel");
const app = express();

function getDate(date){
    const year = date.getFullYear();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const hour = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const datum = day + "." + month + "." + year + " " + hour + ":" + minutes;
    return datum;
}

app.post('/createAction', async(req, res) => {
    try {
        const data = req.body;
        const datum = new Date();
        let date = getDate(datum);
        data.dateCreated = date;
        const user = await loggedModel.findOne({nickname: data.author});
        if (user.length !== 0) {
            const isAdmin = await userModel.find({nickname: data.author, role: "admin"});
            if (isAdmin.length !== 0) {
                const actions = new actionsModel(data);
                await actions.save();
                const attend = new attendeeModel();
                attend.actionID = actions._id;
                attend.attendee = data.author;
                console.log(attend);
                attend.save();
                res.status(200).json("Akce úspěšně přidána.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při přidávání akce!");
    }
});

app.post('/getActions', async(req, res) => {
    try {
        const actions = await actionsModel.find();
        res.status(200).json(actions);
    } catch {
        res.status(400).json("Chyba při načítání akce!");
    }
});

app.post('/deleteAction', async (req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.user});
        if (user.length !== 0) {
            const isAdmin = await userModel.find({nickname: data.user, role: "admin"});
            if (isAdmin.length !== 0) {
                await actionsModel.deleteOne({_id: data.id});
                await attendeeModel.deleteMany({actionID: data.id})
                await actionCommentModel.deleteMany({actionID: data.id})
                res.status(200).json("Akce úspěšně odstraněna.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při odstranění akce!");
    }
});

app.post('/getActionDetails', async(req,res) => {
    try {
        const data = req.body;
        const user = await loggedModel.findOne({nickname: data.user});
        if (user.length !== 0) {
            const action = await actionsModel.findOne({_id: data.id});
            res.status(200).json(action);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při načítání detailů akce!");
    }
});


app.post('/updateAction', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    try {
        const user = await loggedModel.findOne({nickname: data.updateAuthor});
        if (user.length !== 0) {
            const isAdmin = await userModel.find({nickname: data.updateAuthor, role: "admin"});
            if (isAdmin.length !== 0) {
                await actionsModel.updateOne({_id: data.id},
                    {
                        actionName: data.actionName,
                        text: data.text,
                        date: data.date,
                        place: data.place,
                        image: data.image,
                        updateAuthor: data.updateAuthor,
                        updateTime: date
                    });
                res.status(200).json("Akce úspěšně upravena.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při úpravě akce!");
    }
});

app.post('/attend', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.user});
        if (user.length !== 0) {
            const attend = new attendeeModel();
            attend.actionID = data.actionId;
            attend.attendee = data.user;
            console.log(attend);
            attend.save();
            res.status(200).json("Úspěšně přihlášeno na akci.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při přihlášení na akci");
    }
});

app.post('/getAttendees', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0) {
            const attendees = await attendeeModel.aggregate([
                { $match: {
                        actionID: data.id
                    }
                },
                {   $lookup: {
                        from: 'users',
                        localField: 'attendee',
                        foreignField: 'nickname',
                        as: 'user'
                    }
                }]);
            res.status(200).json(attendees);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při získání přihlášených uživatelů");
    }
});

app.post('/cancelAttend', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0) {
            await attendeeModel.deleteOne({attendee: data.nickname, actionID: data.id});
            res.status(200).json('Úspěšně odhlášeno z účasti.');
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při zručšní účasti");
    }
});

app.post('/amiAttending', async(req, res) => {
    const data = req.body;
    try {
        const attendee = await attendeeModel.findOne({attendee: data.nickname, actionID: data.id});
        let attending = null;
        if (attendee !== null){
            attending = true;
        } else {
            attending = false;
        }
        res.status(200).json(attending);
    } catch {
        res.status(400).json("Chyba při získání přihlášených uživatelů");
    }
});

app.post('/addActionsComment', async(req, res) => {
    const data= req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const comment = new actionCommentModel(data);
            await comment.save();
            res.status(200).json("Komentář úspěšně přidán.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/loadActionComments', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const comments = await actionCommentModel.aggregate([
                { $match: {
                        actionID: data.id
                    }
                },
                {   $lookup: {
                        from: 'users',
                        localField: 'nickname',
                        foreignField: 'nickname',
                        as: 'user'
                    }
                }]);
            res.status(200).json(comments);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/updateActionComment', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.updateDate = date;
    console.log(data);
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            if (user.nickname === data.nickname){
                await actionCommentModel.updateOne({_id: data.id}, {text: data.text, updateDate: data.updateDate});
                res.status(200).json("Komentář úspěšně upraven.");
            } else {
                res.status(400).json("Uživatel nemá práva k upravení komentáře.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});


app.post('/deleteActionComment', async(req,res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            if (user.nickname === data.nickname || user.role === 'admin'){
                await actionCommentModel.deleteOne({_id: data.id});
                res.status(200).json("Komentář úspěšně odstraněn.");
            } else {
                res.status(400).json("Uživatel nemá práva k odstranění komentáře.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

module.exports = app;