const express = require("express");
const loggedModel = require("../models/loggedInModel");
const applicantModel = require("../models/applicantModel");
const userModel = require("../models/userModel");
const commentsModel = require("../models/applicantCommentModel");
const app = express();

function getDate(date){
    const year = date.getFullYear();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const hour = date.getHours();
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const datum = day + "." + month + "." + year + " " + hour + ":" + minutes;
    return datum;
}

app.post('/newApplication', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            console.log(data);
            const application = new applicantModel(data);
            const datum = new Date();
            let date = getDate(datum);
            application.date = date;
            await application.save();
            res.status(200).json("Přihláška úspěšně podána.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getUserApplications', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const applications = await applicantModel.find({nickname: data.nickname});
            res.status(200).json(applications);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getApplicationDetail', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const application = await applicantModel.findOne({_id: data.id});
            res.status(200).json(application);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/cancelApplication', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const canBeCancelled = await applicantModel.findOne({_id: data.id, status: "Podána"});
            if (canBeCancelled.length !== 0) {
                await applicantModel.updateOne({_id: data.id}, {status: 'Zrušena uživatelem'});
                res.status(200).json("Aplikace úspěšně zrušena.");
            } else {
                res.status(400).json("Přihláška již nemůže být zrušena");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getOpenApplications', async(req,res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const applications = await applicantModel.find({$or: [{status: 'Podána'}, {status: 'Vyřizuje se'}]});
                res.status(200).json(applications);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/getCompletedApplications', async(req,res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                const applications = await applicantModel.find({$or: [{status: 'Zrušena uživatelem'}, {status: 'Přijata'}, {status: 'Zamítnuta'}]});
                res.status(200).json(applications);
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/changeApplicationStatus', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const isAdmin = await userModel.find({nickname: data.nickname, role: "admin"});
            if (isAdmin.length !== 0) {
                await applicantModel.updateOne({_id: data.id}, {status: data.status});
                res.status(200).json("Stav žádosti úspěšně změněn.");
            } else {
                res.status(400).json("Uživatel není admin!");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/postComment', async(req,res) => {
    const data= req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.date = date;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            const comment = new commentsModel(data);
            await comment.save();
            res.status(200).json("Komentář úspěšně přidán.");
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/loadComments', async (req, res) => {
    console.log('here');
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        console.log('loadingComents')
        if (user.length !== 0){
            const comments = await commentsModel.aggregate([
                { $match: {
                        applicationID: data.id
                    }
                },
                {   $lookup: {
                        from: 'users',
                        localField: 'nickname',
                        foreignField: 'nickname',
                        as: 'user'
                    }
                }]);
            res.status(200).json(comments);
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/deleteComment', async(req, res) => {
    const data = req.body;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            if (user.nickname === data.nickname || user.role === 'admin'){
                await commentsModel.deleteOne({_id: data.id});
                res.status(200).json("Komentář úspěšně odstraněn.");
            } else {
                res.status(400).json("Uživatel nemá práva k odstranění komentáře.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

app.post('/updateComment', async(req, res) => {
    const data = req.body;
    const datum = new Date();
    let date = getDate(datum);
    data.updateDate = date;
    try {
        const user = await loggedModel.findOne({nickname: data.nickname});
        if (user.length !== 0){
            if (user.nickname === data.nickname || user.role === 'admin'){
                await commentsModel.updateOne({_id: data.id}, {text: data.text, updateDate: data.updateDate});
                res.status(200).json("Komentář úspěšně upraven.");
            } else {
                res.status(400).json("Uživatel nemá práva k upravení komentáře.");
            }
        } else {
            res.status(400).json("Uživatel není přihlášen!");
        }
    } catch {
        res.status(400).json("Chyba při ověření uživatele!");
    }
});

module.exports = app;