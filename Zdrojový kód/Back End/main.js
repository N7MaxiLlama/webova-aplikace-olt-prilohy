const express = require('express');
const app = express();
const port = 3000;
const mongoose = require('mongoose');
const discussion = require('./routes/disscussionRoutes');
const daybreak = require('./routes/daybreakRoutes');
const actions = require('./routes/plannedActionsRoutes');
const applicants = require('./routes/applicantRoutes');
const news = require('./routes/newsRoutes');
const users = require('./routes/userRoutes');
const other = require('./routes/otherRoutes');
const guides = require('./routes/guideRoutes');
const cors = require("cors");


app.use(cors({
    origin: ['http://localhost:8080']
}))

app.use(express.json());



//Lokalni testovaci databaze
mongoose.connect('mongodb://127.0.0.1:27023/OLT',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
    console.log("Databáze úspěšně připojena.");
});

app.use(discussion);
app.use(daybreak);
app.use(actions);
app.use(applicants);
app.use(news);
app.use(users);
app.use(other);
app.use(guides);

app.listen(port, () => {
    console.log("Backend server komunity OLT nyní běží na portu " + port + ".")
})