const mongoose = require("mongoose");

const ThemeReportSchema  = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    reportedUserName: {
        type: String,
        required: true,
    },
    reportedText: {
        type: String,
        required: true,
    },
    reportedThemeName: {
      type: String,
      required: true,
    },
    reportText: {
        type: String,
        required: true,
    },
    reportedCommentID: {
        type: String,
        required: true,
    },
    reportDate: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    }
});

const ThemeReport = mongoose.model("ThemeReport", ThemeReportSchema);
module.exports = ThemeReport;