const mongoose = require("mongoose");

const DiscussionSchema  = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    updateDate: {
        type: String,
    },
    updateAuthor: {
        type: String,
    },
    lastReply: {
        type: String,
    }
});

const Discussion = mongoose.model("Discussion", DiscussionSchema);
module.exports = Discussion;