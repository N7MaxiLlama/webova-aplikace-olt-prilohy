const mongoose = require("mongoose");

const LogggedInSchema  = new mongoose.Schema({
    nickname: {
        type: String,
        required: true,
    }
});

const Logged = mongoose.model("Logged", LogggedInSchema);
module.exports = Logged;