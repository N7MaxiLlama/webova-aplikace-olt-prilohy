const mongoose = require("mongoose");

const ReplyReportSchema  = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    reportedUserName: {
      type: String,
      required: true,
    },
    reportedText: {
      type: String,
      required: true,
    },
    reportText: {
        type: String,
        required: true,
    },
    reportedCommentID: {
      type: String,
      required: true,
    },
    reportDate: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    }
});

const ReplyReport = mongoose.model("ReplyReport", ReplyReportSchema);
module.exports = ReplyReport;