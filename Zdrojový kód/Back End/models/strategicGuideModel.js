const mongoose = require("mongoose");

const StrategicSchema = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    updateAuthor: {
        type: String,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    updateDate: {
        type: String,
    },
    image: {
        type: String,
        required: true,
    }
});

const Strategic = mongoose.model("Strategic", StrategicSchema);
module.exports = Strategic;