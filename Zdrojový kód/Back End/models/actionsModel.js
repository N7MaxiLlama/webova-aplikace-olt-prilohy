const mongoose = require("mongoose");

const ActionsSchema = new mongoose.Schema({
    actionName: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    place: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    dateCreated: {
        type: String,
        required: true
    },
    updateAuthor: {
        type: String,
    },
    updateTime: {
        type: String
    }
});

const Actions = mongoose.model("Actions", ActionsSchema);
module.exports = Actions;