const mongoose = require("mongoose");

const AttendeesSchema = new mongoose.Schema({
    actionID: {
        type: String,
        required: true,
    },
    attendee: {
        type: String,
        required: true,
    },
});

const Attendees = mongoose.model("Attendees", AttendeesSchema);
module.exports = Attendees;