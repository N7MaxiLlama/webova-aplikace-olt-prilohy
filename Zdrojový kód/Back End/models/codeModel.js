const mongoose = require("mongoose");

const CodesSchema  = new mongoose.Schema({
    code: {
        type: String,
        required: true,
    },
    faction: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
    }
});

const Codes = mongoose.model("Codes",CodesSchema);
module.exports = Codes;