const mongoose = require("mongoose");

const ActionsCommentSchema = new mongoose.Schema({
    actionID: {
        type: String,
        required: true,
    },
    nickname: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true
    },
    updateDate: {
        type: String,
    },
});

const ActionsComment = mongoose.model("ActionsComment", ActionsCommentSchema);
module.exports = ActionsComment;