const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    nickname: {
        type: String,
        required: true,
        unique: true,
    },
    securityAnswer: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
    },
    profilePicture: {
        type: String,
        required: true,
    },
    VS: {
        type: String,
    },
    TR: {
        type: String,
    },
    NC: {
        type: String,
    },
    NS: {
        type: String,
    },
    aboutMe: {
        type: String,
    }
});

const User = mongoose.model("User", UserSchema);
module.exports = User;