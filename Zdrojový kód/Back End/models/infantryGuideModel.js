const mongoose = require("mongoose");

const InfantrySchema = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    updateAuthor: {
        type: String,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    updateDate: {
        type: String,
    },
    image: {
      type: String,
        required: true,
    },
    infantry: {
        type: String,
        required: true,
    },
    weapon: {
        type: String,
    },
    secondaryWeapon: {
        type: String,
    },
    tool: {
        type: String,
    },
    ability: {
        type: String,
    },
    suit: {
        type: String,
    },
    grenade: {
        type: String,
    },
    utility: {
        type: String,
    },
    melee: {
        type: String,
    },
    implant: {
        type: String,
    },
    secondImplant: {
        type: String,
    },
    tactical: {
        type: String,
    }
});

const Infantry = mongoose.model("Infantry", InfantrySchema);
module.exports = Infantry;