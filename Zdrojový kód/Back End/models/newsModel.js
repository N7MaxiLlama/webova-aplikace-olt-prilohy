const mongoose = require("mongoose");

const NewsSchema  = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    postName: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    picture: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    updateAuthor: {
        type: String,
    },
    updateDate: {
        type: String,
    },
});

const News = mongoose.model("News", NewsSchema);
module.exports = News;