const mongoose = require("mongoose");

const ApplicantCommentSchema = new mongoose.Schema({
    applicationID: {
      type: String,
      required: true,
    },
    nickname: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true
    },
    updateDate: {
        type: String,
    },
});

const ApplicantComment = mongoose.model("ApplicantComment", ApplicantCommentSchema);
module.exports = ApplicantComment;