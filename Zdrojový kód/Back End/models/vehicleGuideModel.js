const mongoose = require("mongoose");

const VehicleSchema = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    updateAuthor: {
        type: String,
    },
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    updateDate: {
        type: String,
    },
    vehicle: {
        type: String,
        required: true,
    },
    weapon: {
        type: String,
    },
    harvest: {
        type: String,
    },
    topGun: {
        type: String,
    },
    secondTopGun: {
        type: String,
    },
    wings: {
        type: String,
    },
    bellyGun: {
        type: String,
    },
    tailGun: {
        type: String,
    },
    leftGun: {
        type: String,
    },
    rightGun: {
        type: String,
    },
    frontLeftGun: {
        type: String,
    },
    frontRightGun: {
        type: String,
    },
    backLeftGun: {
        type: String,
    },
    backRightGun: {
        type: String,
    },
    utility: {
        type: String,
    },
    defence: {
        type: String,
    },
    performance: {
        type: String,
    },
    logistics: {
        type: String,
    }
});

const Vehicle = mongoose.model("Vehicle", VehicleSchema);
module.exports = Vehicle;