const mongoose = require("mongoose");

const ApplicantSchema = new mongoose.Schema({
    nickname: {
        type: String,
        required: true,
    },
    vsChar: {
        type: String,
    },
    trChar: {
        type: String,
    },
    howLong: {
        type: String,
        required: true,
    },
    style: {
        type: String,
        required: true,
    },
    whyUs: {
        type: String,
        required: true,
    },
    note: {
        type: String
    },
    status: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true
    }
});

const Applicant = mongoose.model("Applicant", ApplicantSchema);
module.exports = Applicant;