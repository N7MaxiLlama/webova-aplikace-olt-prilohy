const mongoose = require("mongoose");

const DiscussionReplySchema  = new mongoose.Schema({
    author: {
        type: String,
        required: true,
    },
    text: {
        type: String,
        required: true,
    },
    date: {
        type: String,
        required: true,
    },
    updateDate: {
        type: String,
    },
    quoteAuthor: {
        type: String,
    },
    quoteDate: {
        type: String,
    },
    quoteText: {
      type: String,
    },
    discussionID: {
        type: String
    }
});

const DiscussionReply = mongoose.model("DiscussionReply", DiscussionReplySchema);
module.exports = DiscussionReply;