import { defineStore } from 'pinia'
import axios from "axios";
import config from "../config";

export const useInfantryGuideStore = defineStore('infantryGuide', {
    state: () => ({
        error: null,
        guides: [],
        lastID: null,
        role: null,
        guideDetail: null,
    }),

    actions: {
        async addGuide(nickname, name, description, text, infantry, weapon, secondaryWeapon, tool, ability, suit, grenade, utility, melee, implant, secondImplant, tactical, image){
            try {
                const results = await axios.post(config.backendUrl + '/addInfantryGuide', {
                    author: nickname,
                    updateAuthor: null,
                    name: name,
                    description: description,
                    text: text,
                    date: null,
                    updateDate: null,
                    infantry: infantry,
                    weapon: weapon,
                    secondaryWeapon: secondaryWeapon,
                    tool: tool,
                    ability: ability,
                    suit: suit,
                    grenade: grenade,
                    utility: utility,
                    melee: melee,
                    implant: implant,
                    secondImplant: secondImplant,
                    tactical: tactical,
                    image: image,
                });
                this.lastID = results.data;
                this.error = false;
            } catch {
                this.error = true;
            }
        },

        async getGuides(){
            try {
                const results = await axios.get(config.backendUrl + '/getInfantryGuides', {

                });
                this.guides = results.data;
                this.error = false;
            } catch {
                this.error = true;
            }
        },

        async getGuideDetail(id){
            try {
                const response = await axios.post(config.backendUrl + '/infantryDetail', {
                    ID: id,
                });
                this.guideDetail = response.data;
                this.error = false;
            } catch {
                this.error = true;
            }
        },

        async updateGuide(nickname, name, description, text, weapon, secondaryWeapon, tool, ability, suit, grenade, utility, melee, implant, secondImplant, tactical, image){
            try {
                await axios.post(config.backendUrl + '/updateInfantryGuide', {
                    id: this.guideDetail._id,
                    updateAuthor: nickname,
                    name: name,
                    description: description,
                    text: text,
                    updateDate: null,
                    weapon: weapon,
                    secondaryWeapon: secondaryWeapon,
                    tool: tool,
                    ability: ability,
                    suit: suit,
                    grenade: grenade,
                    utility: utility,
                    melee: melee,
                    implant: implant,
                    secondImplant: secondImplant,
                    tactical: tactical,
                    image: image,
                });
                this.error = false;
            } catch {
                this.error = true;
            }
        },

        async deleteGuide(nickname){
            try {
                await axios.post(config.backendUrl + '/deleteInfantryGuide',{
                    id: this.guideDetail._id,
                    nickname: nickname,
                });
                this.error = false;
            } catch {
                this.error = true;
            }
        },

    },

    persist: true
})